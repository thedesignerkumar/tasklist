<?php 
  
  $page = basename($_SERVER['PHP_SELF'], '.php'); // find the file name for the currently displayed page  

 ?>

<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="utf-8">
    <title><?php echo $page_title ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>

    <header>

        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Task List</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li <?php if ($page == 'home') echo 'class="active"'; ?>><a href="home.php">Home <span class="sr-only">(current)</span></a></li>
                <li <?php if ($page == 'add-task') echo 'class="active"'; ?>><a href="add-task.php">Add Task</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

    </header>
    
    <main class="container">
    <h1 class="page-header"><?php echo $page_header; if (isset($badge)) echo ' <span class="badge">' . $badge . '</span>'?></h1>