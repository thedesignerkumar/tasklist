<?php
    
    // start the session
    session_start();

    // if id doesnt exists, return the user to the home page
    if (!isset($_GET['id'])) {
        header('Location: home.php');
        exit();
    }

    // include the header
    $page_header = 'Edit Task';
    $page_title = 'Edit task';
    include_once 'includes/header.php';

    // call the database handler
    require_once 'includes/db_connect.php';

    // sanitize the id
    $id = htmlspecialchars(trim($_GET['id']));

    // get the details of the task for the asked id
    $stmt = $db->prepare("SELECT title, description FROM tasks WHERE task_id=:id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    // if details found in the database, define the details variable
    if ($rows = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $title = $rows['title'];
        $desc = $rows['description'];
    } else {
        // if details not found in the database, return the error message
        echo '<p>No task found for id: ' . $id . '</p>';
        
        // display link to home page
        echo '<a href="home.php">Back to home</a>';
        
        // include page footer
        include_once 'includes/footer.php';

        // kill the page
        exit();
    }

    // show the page content only if the details found in the database
    if ($title):

    // if the form handler has set the information about the query, display the message accordingly
    if (isset($_SESSION['message'])) {
        if ($_SESSION['message'] == 'success') {
            echo '<div class="alert alert-success" role="alert">Task updated successfully</div>';
        } elseif ($_SESSION['message'] == 'failure') {
            echo '<div class="alert alert-warning" role="alert">Nothing to update!</div>';
        }
    }

?>

 <form method="post" action="form_handler.php">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php if (isset($title) && !isset($_SESSION['errors']['title'])) echo $title ?>">
        <!-- Display the error message if the form handler returned errors -->
        <?php if(isset($_SESSION['errors']['title'])) echo '<p class="help-block"><span class="text-danger">' . $_SESSION['errors']['title'] . '</span></p>'; ?>
    </div>
    <div class="form-group">
        <label for="desc">Description</label>
        <textarea class="form-control" id="desc" name="desc" placeholder="Description of your task"><?php if (isset($desc) && !isset($_SESSION['errors']['desc'])) echo $desc ?></textarea>
        <!-- Display the error message if the form handler returned errors -->
        <?php if(isset($_SESSION['errors']['desc'])) echo '<p class="help-block"><span class="text-danger">' . $_SESSION['errors']['desc'] . '</span></p>'; ?>
    </div>  
    <button type="submit" class="btn btn-default">Submit</button>
    <input type="hidden" name="submitted" value="true">
    <input type="hidden" name="form_type" value="edit_task">
    <input type="hidden" name="task_id" value="<?php echo $id ?>">
</form>


<?php

    endif; // close the if statement for displaying the page contents

    // clear all session variables
    session_unset();

    // include the footer
    include_once 'includes/footer.php';
    
 ?>
