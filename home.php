<?php 


    // call the database handler
    require_once('includes/db_connect.php');

    // set the number of records to display
    $records = 5;

    // find out how many records are their
    $stmt = $db->query("SELECT COUNT(task_id) FROM tasks");
    if ($stmt->rowCount()) {
        $total_records = $stmt->fetch(PDO::FETCH_NUM);
        $total_records = $total_records[0];
    } else {
        echo '<p>No records found</p>';
    }

    // close the connection
    $stmt->closeCursor();
    $stmt = null;

    // determine the total pages required
    $pages = ceil($total_records / $records);

    // set the page number
    $page_num = 1;
    // if the start is defined in the url then use that as the start count
    if (isset($_GET['pn'])) {
        $page_num = $_GET['pn'];
    }

    // page number cannot be more than required pages or less than 1
    if ($page_num > $pages) {
        $page_num = $pages;
    } elseif ($page_num < 1) {
        $page_num = 1;
    }

    // determine the start value for limit
    $start = ($page_num -1) * $records;

    $page_ttle = 'Task Manager';
    $page_header = "All Tasks";
    $badge = $total_records;
    include_once 'includes/header.php';

 ?>

<!-- Display the table -->
 <div class="table-responsive">
     <table class="table table-hover table-stripped">
         <tr>
             <th>Title</th>
             <th>Description</th>
             <th>Date Created</th>
             <th>Edit</th>
             <th>Delete</th>
         </tr>

         <tbody>
             
             <?php 

                // build the query
                $stmt = $db->prepare("SELECT task_id, title, description, DATE_FORMAT(date_added, '%M %D, %Y') AS date FROM tasks ORDER BY date_added DESC LIMIT $start, $records");

                // execute the query
                  $stmt->execute();

                // Display results from the database
                if ($stmt->columnCount()) {
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                        
                        <tr>
                            <td><?php echo $row['title'] ?></td>
                            <td><?php echo $row['description'] ?></td>
                            <td nowrap="nowrap"><?php echo $row['date'] ?></td>
                            <td><a href="edit-task.php?id=<?php echo $row['task_id']; ?>">Edit</a></td>
                            <td><a href="delete-task.php?id=<?php echo $row['task_id']; ?>">Delete</a></td>
                        </tr>

                        <?php
                    }
                } else {
                    echo '<p>There are no tasks defined</p>';
                }

                // close the database connection
                $stmt->closeCursor();
                $stmt = null;
                $db = null;

              ?>

         </tbody>
     </table>
 </div>

 <!-- PAGINATION -->
 <nav class="text-center" aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="home.php?pn=<?php echo $page_num - 1 ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <?php 

        // display the page number
        for ($i=1; $i <= $pages; $i++) { 
            echo "<li><a href=\"home.php?pn={$i}\">{$i}</a></li>";
        }

     ?>
    <li>
      <a href="home.php?pn=<?php echo $page_num + 1 ?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>


<?php

    // include the page footer
    include_once 'includes/footer.php';
?>

 