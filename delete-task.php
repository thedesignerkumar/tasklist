<?php

    // if id doesnt exists, return the user to the home page
    if (!isset($_GET['id'])) {
        header('Location: home.php');
        exit();
    }

    // include the page header
    $page_title = 'Delete Task';
    $page_header = 'Confirm Deletion';
    include_once 'includes/header.php';

    // call the database handler
    require_once('includes/db_connect.php');

    // sanitize the id
    $id = htmlentities(trim($_GET['id']));

    // build the query
    $stmt = $db->prepare("SELECT title FROM tasks WHERE task_id=:id");
    $stmt->bindParam(':id', $id);


    // execute the query
    $stmt->execute();


    // Fech the title of the task to be deleted and set it to the $title variable 
    if ($rows = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $task_title = $rows['title'];
    } else {
        // display the error if no deails found in the database
        echo 'No task found for the id ' . $id;

        // display link to home page
        echo '<br><br><a href="home.php">Back to home</p>';
        
        //include page footer
        include_once 'includes/footer.php';
        
        // kill the page
        exit();
    }

    $stmt->closeCursor;
    $stmt = null;

    // if the user action form is submitted, process the form
    if (isset($_POST['submitted'])) {
        // create the message array
        $message = array();

        // if user selected yes, then delete the task and inform user
        if ($_POST['confirm'] == 'yes') {
            // build the query
            $stmt = $db->prepare("DELETE FROM tasks WHERE task_id=:id");
            
            // execute the query
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            // inform the user about the result of the query
            if ($stmt->rowCount()) {
                echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Task ' . $title . ' deleted successfully!</div>';
            } else {
                echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Task could not be deleted at this time. Please try later</div>'; 
            }
        } else { // if the user selected no, then dont process any database query and display the message
            echo '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Task deletion cancelled!</div>';                
        }

        // display link to the home page
        echo '<a href="home.php">Back to Home</a>';
    } else {
        // the form is not submitted, display the user action form
        ?>

            <form method="post" action="">
                <div class="form-group">
                    <h3>Task Title: <?php echo $task_title ?> </h3>
                    <h4 class="text-danger">Do you really want to delete this task?</h4>
                </div>
                <div class="radio">
                    <label>
                    <input type="radio" name="confirm" id="confirm" value="yes">
                        Yes
                    </label>
                </div>
                    <div class="radio">
                    <label>
                    <input type="radio" name="confirm" id="confirm" value="no" checked>
                        No
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <input type="hidden" name="submitted" value="true">
            </form>

        <?php
    }

    // include the page footer
    include_once 'includes/footer.php';

 ?>