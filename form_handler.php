<?php

// start the session
session_start();

// if any form is not submitted then return the user to the home page
if (!isset($_POST['submitted'])) {
    header('Location: home.php');
    exit();
}

// process the form

// set the form type in order to decide whether to update a current record or insert a new record in database
$form_type = $_POST['form_type'];

// set the header url to determine whichpage to redirect based on the form type received 
$header_url = ($form_type == 'add_task') ? 'add-task.php' : 'edit-task.php?id=' . $_POST['task_id'] ;


// Validate the form fields

// include the validate class
require_once('classes/Validate.php');

$validate = new Validate;
$validate->addRequired(array('title', 'desc'));
$validate->validateRequired($_POST);

// if input fields are not valid
if (!$validate->isValid()) {
    $errors = $validate->getErrors();
    $_SESSION['errors'] = $errors;
    $_SESSION['title'] = $_POST['title'];
    $_SESSION['desc'] = $_POST['desc'];
    header("Location: $header_url");
} else {

    // if all the required fields are provided, proceed with form submission
     // call the database handler
    require_once('includes/db_connect.php');

    // Sanitize the user input
    $title = htmlspecialchars(trim($_POST['title']));
    $desc = htmlspecialchars(trim($_POST['desc']));
    $id = htmlspecialchars(trim($_POST['task_id']));

    // Build the necessary query according to the form type 
    if ($form_type == 'add_task') {
        $stmt = $db->prepare("INSERT INTO tasks (title, description, date_added) VALUES (:title, :description, NOW())");
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $desc);
    } elseif ($form_type == 'edit_task') {
        $stmt = $db->prepare("UPDATE tasks SET title=:title, description=:description WHERE task_id=:id");
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $desc);
        $stmt->bindParam(':id', $id);
        $_SESSION['title'] = $title;
    }



    // bind the parameters and execute the query
    $stmt->execute();

    // return the information if the query was successful or not
    if ($stmt->rowCount()) {
        $_SESSION['message'] = 'success';
    } else {
        $_SESSION['message'] = 'failure';
    }

    // close the database connection and return to the appropriate page
    $stmt->closeCursor();
    $stmt = null;
    $db = null;
    header("Location: $header_url");
}