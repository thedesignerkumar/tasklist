<?php

class Validate
{
    protected $required_fields = array();
    protected $errors = array();

    /**
     * Add fields to the required array.
     * This function adds the array of fields to the required array
     * @param array $required All fields that are required
     * @return void
     */
    public function addRequired(array $required)
    {
        $this->required_fields = $required;
    }

    /**
     * Validate inputs against required fields.
     * This function checks the inputs against the required fields, if the input is empty and defined 
     * in the required field, then the $errors array is populated with appropriate error message
     * @param array $fields Input fields which needs to be validated
     * @return void
     */
    public function validateRequired(array $fields)
    {
        foreach ($this->required_fields as $key) {
                echo $key . '<br>';
                if (empty($fields[$key])) {
                    $this->errors[$key] = $key . ' is required';
            }
        }
    }

    /**
     * Returns validation results
     * This function checks if the $errors array is empty or not. If it is empty then the function returns
     * false (not error), otherwise it returns the $errors array. This function accepts no parameter
     * @return boolean
     */
    public function isValid()
    {
        if (!empty($this->errors)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns error messages.
     * This function returns array of error messages, if generated after the input validations. This function
     * accepts no parameter
     * @return array;
     */
    public function getErrors()
    {
        if (!empty($this->errors)) {
            return $this->errors;
        } else {
            throw new Exception('No error found');            
        }
    }
}

