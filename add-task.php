<?php 
    
    // start the session
    session_start();
    
    // include the page header
    $page_title = "Add Task";
    $page_header = "Add a new task";
    include_once 'includes/header.php';

    // if the form handler returned query results, display the message
    if (isset($_SESSION['message'])) {
        if ($_SESSION['message'] == 'success') {
            echo '<div class="alert alert-success" role="alert">New task added successfully!</div>';
        } elseif ($_SESSION['message'] == 'failure') {
            echo '<div class="alert alert-danger" role="alert">Sorry, your task could not be added. Please try again later</div>';
        }
    }

 ?>

 <form method="post" action="form_handler.php">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php if (isset($_SESSION['title'])) echo $_SESSION['title'] ?>">
        <!-- if the form handler returned errors, display the error -->
        <?php if(isset($_SESSION['errors'])) echo '<p class="help-block"><span class="text-danger">' . $_SESSION['errors']['title'] . '</span></p>'; ?>
    </div>
    <div class="form-group">
        <label for="desc">Description</label>
        <textarea class="form-control" id="desc" name="desc" placeholder="Description of your task"><?php if (isset($_SESSION['desc'])) echo $_SESSION['desc'] ?></textarea>
        <!-- if the form handler returned errors, display the error -->
        <?php if(isset($_SESSION['errors'])) echo '<p class="help-block"><span class="text-danger">' . $_SESSION['errors']['desc'] . '</span></p>'; ?>
    </div>  
    <button type="submit" class="btn btn-default">Submit</button>
    <input type="hidden" name="submitted" value="true">
    <input type="hidden" name="form_type" value="add_task">
</form>

<?php

    // clear the session
    session_unset();

    // include the page footer
    include_once 'includes/footer.php';
    
 ?>