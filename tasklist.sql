-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tasklist
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `task_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (43,'Animi dolor pariatur soluta.','Dolor qui hic quidem maxime perferendis quisquam repellat numquam id praesentium ex inventore placeat nihil non aut autem deserunt eos dolorum aut quisquam at iste autem dolore qui.','2012-11-24 15:09:14'),(44,'Iusto nemo qui quam.','Praesentium temporibus ex laboriosam autem consectetur earum error sed architecto nesciunt facere enim earum facere voluptatibus sunt vel quaerat et quia cupiditate non sed quod.','2001-06-29 03:41:20'),(45,'Eum non corrupti maiores voluptas saepe.','Et rerum est a recusandae possimus qui accusantium laudantium dolor voluptatem et et nihil inventore sed non odit.','1980-03-03 01:41:56'),(46,'Quo repudiandae assumenda voluptates.','Tempora enim ut quia suscipit aut doloribus iusto odio beatae ea molestiae recusandae totam sunt fuga est voluptatem quia quaerat.','1994-09-12 08:03:27'),(47,'Dolorem nobis eaque sapiente sunt.','Nisi veritatis porro non vel omnis repellat temporibus qui quasi eaque esse asperiores architecto dolorum reiciendis animi quasi corrupti voluptatum doloribus et fuga facilis.','2014-03-09 14:26:10'),(49,'Voluptas veritatis vel modi aliquam et pariatur.','Quia consequatur voluptatibus atque ab aut voluptatum nemo est totam quia ut in consectetur saepe rerum deleniti et consequatur rem.','2009-11-04 14:37:59'),(50,'Enim sapiente omnis autem adipisci quia accusantium.','Aut et dicta sed suscipit quidem et quidem in in eum eum quod quia eaque labore.','1977-02-07 21:02:15'),(51,'Expedita voluptatum nobis qui quia cum omnis.','Culpa reiciendis voluptas minima perferendis ut labore voluptates eos aut praesentium amet quidem et praesentium cupiditate impedit totam quas hic est et sapiente nisi omnis ea.','2013-04-27 01:44:26'),(52,'Vel explicabo et animi incidunt.','Iusto velit qui distinctio impedit dolorem molestiae doloribus perspiciatis ut ut aut sed voluptas voluptate non nam odit quisquam totam quidem sunt autem magnam.','2011-12-24 07:49:12'),(53,'Amet omnis in eligendi quidem.','Consequatur occaecati ratione mollitia non eius optio consectetur tempora et ullam laboriosam commodi repellendus dolor id numquam ut ea cumque et quia expedita.','1971-02-17 19:37:34'),(54,'Fugiat et saepe labore quam perferendis.','Tenetur sapiente est cupiditate optio impedit molestiae fuga vel illum amet enim aut facilis magni est cupiditate sint expedita et nesciunt nam qui fugiat reiciendis incidunt.','1994-04-24 05:52:06'),(55,'Velit maiores porro fugiat voluptatem id non.','Illum minima veritatis excepturi nulla cum eius ut nobis nobis fuga nostrum et sequi illum hic voluptas nostrum nisi dolorem facilis et nemo cupiditate sit dolorem nemo dolore.','1988-12-20 01:39:33'),(56,'Iure consequatur autem sit placeat eum repudiandae.','Magni dolorem omnis libero voluptatum illum amet qui quae unde deleniti sint praesentium debitis.','1995-09-13 08:31:29'),(57,'Explicabo voluptatibus nesciunt esse et.','Qui saepe numquam unde dignissimos quasi sed animi fugiat perferendis quidem quia repellat in delectus cupiditate perspiciatis porro asperiores sit vel consequatur non libero nostrum repellendus ipsa.','1975-02-25 01:47:44'),(58,'Recusandae quam dolorem eligendi aut doloremque.','Impedit quo iusto aut quia velit itaque maiores explicabo sequi et modi aut vel omnis rem qui ea qui doloremque quibusdam laudantium impedit sit.','1980-05-28 22:46:48'),(60,'A new task','Hello, this is a test task','2016-12-25 11:25:06'),(61,'Another task','Hello there. This is the next task','2016-12-25 15:52:59');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-25 17:15:21
