<?php

    // define('DB_NAME', 'tasklist');
    // define('DB_HOST', '127.0.0.1');
    // define('DB_USER', 'root');
    // define('DB_PASS', 'root');

    // $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // if (!$db) {
    //     echo 'Unable to connect to database';
    //     exit();
    // }
    

$connstr = 'mysql:dbhost=127.0.0.1;dbname=tasklist';
$user = 'root';
$pass = 'root';

try {
    $db = new PDO($connstr, $user, $pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Could not connect to database" . $e->getMessage();
}